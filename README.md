Ansible Role: Minio in Docker Swarm
=========

Setup minio in docker swarm cluster

Requirements
------------

None

Role Variables
--------------
[defaults/main.yml](defaults/main.yml)

Dependencies
------------

[ansible_role_docker](https://gitlab.com/klovtsov/devops_includes/ansible_roles/ansible_role_docker)
[ansible_role_docker_swarm](https://gitlab.com/klovtsov/devops_includes/ansible_roles/ansible_role_docker_swarm)

Example Playbook
----------------

    - hosts: all
      become: true
      gather_facts: true
      roles:
        - ansible_role_minio_swarm

License
-------

MIT

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
